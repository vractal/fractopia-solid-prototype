# Fractopia

## Intro 

Fractopia intents to be a layer on top of data. Instead of a platform or app, a layer that uses semantic data to define how you see and interact with linked data (and thus, the world).

Short intro video: https://www.youtube.com/watch?v=wtemMRwScJ4

Live demo: https://proto.fractopia.org

- Some notable bugs
  - When you create a note, you need to refresh the screen to see the update on the list

The main technologies being used:

- javascript (hopefully typescript soon)
- vue/vuex
- Vuetify - (for Ui components)
- Solid
  - solid-community-server (others also work)
  - solid-client (inrupt)
  - solid-authn-client-browser (inrupt)

## How to run

In order to use fractopia, you need a solid pod. You can create one in any solid pod provider, like [Solid Community](https://solidcommunity.net/) which is set as default provider at login page for now, or  you can run a pod locally. (Todo: Add more info about runnning pod locally here)

Besides that, you should be a

## Install dependencies
```
yarn install
```

### Start development server (hot-reload)
```
yarn serve
```

### Compile and build for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

## More info about the project
- [Fractopic dream](src/docs/fractopic-dream.md), gives you an example of how this could be used
- [Important concepts](src/docs/important-concepts.md) provides more information on the main building blocks of the system
- [Roadmap](src/docs/Roadmap.md) (Needs to be Improved)