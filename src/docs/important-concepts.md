
### Important concepts in Fractopia

Here we define some concepts that constitute the basis of Fractopia and you are going to see a lot around here.

- Spaces:
    
    - Spaces are Storages where data of a profile or group is stored at. It also contains access control information.
        
- Portals:
    
    - Portals are the interfaces by which people interact with their data. You look networks of data through your portals. This portals can define what types of data/relations you are interested in, how can you act on this data and even the interface. You can configure and share them, with different tools and components. Basically, everything inside a Portal is a Portal as well, that may contain other Portals. Portals are defined by semantic data stored in pods, so they can easily be shared. Any non-tech oriented person should be able to create new portals to visualize and interact with data (and other people, and the world) in their own way.
        
- HyperFolders (in the search for a better name):
    
    - HyperFolders are collections of links to things and their relations. You can see them as folders, but they are really graphs. Your personal graphs, your mind map to the data you care about. They are not bound to regular folders, as they are links that lead to other links and things. So one thing can be in many hyperfolders, and one hyperfolder can contain data from many spaces.
        
- Dynamic vocabularies and shared ontologies
    
    - One of the main goals of Fractopia is a place where anyone can experience the digital space itself as home. That is, not only feel comfortable in a specific social media or forum, but in to the entire network. And that's possible, because in Fractopia we are the crafters of our own network.
        
    - In order to do that, the user must be able to define their own ontologies as easily as they think them but, at the same time, the more connections, the better.
        
    - To conciliate that, besides being able to define new concepts and relations for data easily, people should be able to discuss and share their ontologies, using a mix of personal, local and global vocabularies for instance. Thus we allow for diversity and localization without giving up on integration.