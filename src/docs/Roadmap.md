
# Roadmap and future goals

- Create personalized types of data, such as recipes, musics, books, events, and ways of interacting with them;
- Use apropriate solid related vocabs correctly (typeIndex, containers, etc)
- 
- Include management tasks, forums, and blogs functionalities;
- Create Libraries and Groups tools: ways of sharing and visualizing any kind of data and relations;
- Try integrations with other Solid-powered technologies that already exists;
- Strategic discussion about how to make Fractopia flourish, with a broader community.
s


# Backlog (non exaustive)

## Tools and functionalities:
- [ ] Editor of data in blocks (dokieli?)
- [ ] Libraries
- [ ] Tasks
- [ ] Forum
- [ ] Blogs
- [ ] Environment for collaboratively creating data models and ontologies.
- [ ] Environment for creating relationships between things
- [ ] Tool for creating and sharing portals Portals usable by non-tech savier people
- [ ] Graph vision of data
- [ ] Data vizualization components ex: gráficos, grafos, tabelas, paineis, mapas...
- [ ] Territories: 2D or 3D coolaborative digital territories for more organic interactions online and ways of spacialy represent linked data 
- [ ] Integration with audio/video calls
- [ ] Integration with Matrix Network
- [ ] Contrutor de layouts compartilhaveis pelo usuario final
- [ ] Forma de integrar componentes externos (webcomponents?)

## General
- [ ] Implement end-to-end encryption
- [ ] Multiple personas, easyli changeable/accessable
- [ ] Show related projects, like OpenSource Ecology
- [ ] Create fractopia ontology
- [ ] Create data migration system
- [ ] Save some or all data from a Pod on a device, for offline mode
- [ ] Offline mode

## Portals
- [ ] Define types and structure for creation and display
- [x] Change and create new ones
- [ ] Custom root portals

